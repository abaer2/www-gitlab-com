title: What is a DevOps platform engineer?
description: A bleeding-edge role, DevOps platform engineers fill the space
  between hardware and software.
header_body: A bleeding-edge role, DevOps platform engineers fill the space
  between hardware and software.
canonical_path: /topics/devops/what-is-a-devops-platform-engineer/
file_name: what-is-a-devops-platform-engineer
parent_topic: devops
twitter_image: /images/opengraph/gitlab-blog-cover.png
body: >-
  ## What is a DevOps platform engineer?


  In the complex world that can be software development, a [DevOps platform](/solutions/devops-platform/) that can be deployed as a single application brings all the disparate forces together. It's tough to argue against the benefits of a single solution, but someone has to “own” the platform.


  In this new era of DevOps, enter the DevOps platform engineer, a bleeding-edge role that is popping up on various job listings. Although some would argue a platform engineer is just another name for a [site reliability engineer](https://about.gitlab.com/topics/devops/what-is-a-site-reliability-engineer/), the rise of [cloud native](https://about.gitlab.com/topics/cloud-native/) technologies such as Kubernetes, [microservices](https://about.gitlab.com/topics/microservices/), and containers have pushed some companies to create a [platform engineering team](https://about.gitlab.com/topics/devops/how-and-why-to-create-devops-platform-team/) (or teams) charged with overseeing the platforms and related technologies.


  ## Platform engineers have serious skills


  A look at advertised job responsibilities and qualifications shows how a platform engineer is expected to operate in a DevOps team. In general, a platform engineer's role is to help developers get software out the door more quickly and with security in mind. As such, it's not surprising companies are looking for platform engineers with:


  * [CI/CD](https://about.gitlab.com/topics/ci-cd/) and other automation experience

  * Familiarity with infrastructure as code

  * Familiarity with cloud native technologies such as Kubernetes, Google Cloud, and Docker

  * Extensive experience with cloud deployments

  * Knowledge of secure coding practices including OWASP, secrets management, and vulnerability remediation

  * Strong programming chops and deep familiarity with Linux/Unix operating systems

  * Solid interpersonal skills and a desire to improve the developer experience


  It's important to remember, however, that the responsibilities of a platform engineer could vary widely depending on the type of organization. A greenfield company with no legacy systems is likely to have cloud expertise baked in, while an enterprise (and its presumptive legacy systems) may need extra help when it comes to migrations.


  ## How platform engineers work in a DevOps environment


  There's no question that a platform engineer plays a pivotal role sitting between Dev and Ops, but leaning more toward operations. One company [wrote about their DevOps platform engineering journey](https://medium.com/seek-blog/platform-engineering-why-we-dont-need-a-devops-team-e88c8b97cc4f) and said at the end of the day their focus was on operations and site reliability. Others have suggested a DevOps platform engineer must be responsible for seamless “self-serve” production for developers, as well as monitoring, alerting, and even potentially evangelism for the platform itself.


  It is likely this role will continue to evolve over time as more teams adopt DevOps platforms and take full advantage of them. In our [2022 Global DevSecOps Survey](https://about.gitlab.com/developer-survey/), three-quarters of respondents told us their teams use a DevOps platform or plan to use one this year. Another 21% said they are considering a DevOps platform in the next two to three years.
resources_title: Read more about DevOps platform
resources:
  - title: Give devs the power to deploy and double your efficiency
    type: Case studies
    url: https://about.gitlab.com/customers/glympse/
  - url: https://www.youtube.com/embed/MNxkyLrA5Aw
    type: Video
    title: The benefits of a single application explained in a video (2.5 minutes)
  - title: How the idea of a DevOps platform was born in Poland
    url: https://about.gitlab.com/blog/2020/10/29/gitlab-hero-devops-platform/
    type: Blog
