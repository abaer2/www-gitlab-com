---
layout: markdown_page
title: "Category Direction - User Profile"
description: "The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Learn more!"
canonical_path: "/direction/enablement/user-profile/"
---

- TOC
{:toc}

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Data Stores](/direction/enablement/) | [Not Applicable](/direction/maturity/) | `2023-02-21` |

## Introduction and how you can help

Thanks for visiting this category direction page on the User Profile at GitLab. The User Profile category is part of the [Organization Group](https://about.gitlab.com/handbook/product/categories/#organization-group) within the [Enablement](https://about.gitlab.com/direction/enablement/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute: 
* Please comment in the linked issues and epics on this page. Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy. 
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for the User Profile, we'd love to hear from you. 
* Please open an issue using the ~"Category:User Profile" label, or reach out to us internally in the #g_manage_organization Slack channel.


## Overview

The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Namely:

* User profile: User settings and how a user's profile is shown to the instance/world.

## Target audience and experience

Any user of GitLab could be considered a relevant audience, but improvements in this area likely think about two specific instances:

1. Large self-managed instances: These are customers with large seat counts, who need the ability to understand who is using GitLab and who is not - and have the tools needed to manage these users at scale. 
1. GitLab.com: For individual contributors, the User Profile becomes your identity as a developer. While concepts like the contribution graph or project list may not be of paramount importance to a self-managed enterprise user, individual developers on GitLab.com want their profile and presence on GitLab.com to represent them.

## Maturity

Users are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

## What's next & why

**Next:** Due to other priorities for the [Enablement](https://about.gitlab.com/direction/enablement/) section, we are not planning to make significant investments to the User Profile category in the next 6-8 milestones. 

We are planning improvements to User Profiles in the mid-term to make them more personalized. If you want to contribute to improve the User Profile page, you can add suggestions [in this epic](https://gitlab.com/groups/gitlab-org/-/epics/8488).
